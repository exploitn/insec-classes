﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using System.IO;
using System.Web;
using System.Net;
using System.ComponentModel;

using System.Net.Cache;
// Add a reference to ServiceModel and uncomment the ServiceModel line in POST to remove 
// the default 100 Expect Continue header that .net adds to POST
// using System.ServiceModel;

namespace Classes
{
    /* Http Class
     * - Written by Precise
     * - Managing http GET and POST requests
     */
    class Http
    {
        private CookieContainer cookies;
        private WebHeaderCollection headers;
        private String html, useragent;

        public CookieContainer Cookies { get { return cookies; } set { cookies = value; } }
        public WebHeaderCollection Headers { get { return headers; } set { headers = value; } }
        public String Html { get { return html; } set { html = value; } }
        public String Useragent { get { return useragent; } set { useragent = value; } }

        /* Create a new cookie container
         * - Http http = new Http();
         */
        public Http()
        {
            cookies = new CookieContainer();
            headers = new WebHeaderCollection();
            html = "";
        }

        /* HttpGet(url_, [referer_], [proxy_], [headers_])
         * - Perform an http GET request
         * - Referer, proxy, and headers parameters are optional
         * - HttpGet("https://www.google.com");
         */
        public void HttpGet(String url_, String referer_ = "", String proxy_ = "", WebHeaderCollection headers_ = null)
        {
            HttpWebRequest http = (HttpWebRequest)WebRequest.Create(url_);
            if (referer_ != "" && referer_ != null) { http.Referer = referer_; }
            if (proxy_ != "" && proxy_ != null) { http = HttpProxy(http, proxy_); }
            if (headers_ != null)
            {
                foreach (String headerStr in headers_)
                {
                    http.Headers.Add(headerStr);
                }
            }      
            HttpGetMain(http);
        }

        /* HttpGetAsync(url_, [referer_], [proxy_], [headers_])
         * - Perform an http GET request in its own thread
         * - Referer, proxy, and headers parameters are optional
         * - HttpGetAsync("https://www.google.com");
         */
        public void HttpGetAsync(String url_, String referer_ = "", String proxy_ = "", WebHeaderCollection headers_ = null)
        {
            BackgroundWorker bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.DoWork += new DoWorkEventHandler(
            delegate(object o, DoWorkEventArgs args)
            {
                HttpWebRequest http = (HttpWebRequest)WebRequest.Create(url_);
                if (referer_ != "" && referer_ != null) { http.Referer = referer_; }
                if (proxy_ != "" && proxy_ != null) { http = HttpProxy(http, proxy_); }
                if (headers_ != null)
                {
                    foreach (String headerStr in headers_)
                    {
                        http.Headers.Add(headerStr);
                    }
                }
                HttpGetMain(http);
            });
            bw.RunWorkerAsync();
        }

        /* HttpPost(url_, data_, [referer_], [proxy_], [headers_])
         * - Perform an http POST request in its own thread
         * - Referer, proxy, and headers parameters are optional
         * - HttpPost("http://httpbin.org/", "data=value");
         */
        public void HttpPost(String url_, String data_, String referer_ = "", String proxy_ = "", WebHeaderCollection headers_ = null)
        {
            HttpWebRequest http = (HttpWebRequest)WebRequest.Create(url_);
            if (referer_ != "" && referer_ != null) { http.Referer = referer_; }
            if (proxy_ != "" && proxy_ != null) { http = HttpProxy(http, proxy_); }
            if (headers_ != null)
            {
                foreach (String headerStr in headers_)
                {
                    http.Headers.Add(headerStr);
                }
            }
            HttpPostMain(http, data_);
        }


        /* HttpPostAsync(url_, data_, [referer_], [proxy_], [headers_])
         * - Perform an http POST request in its own thread
         * - Referer, proxy, and headers parameters are optional
         * - HttpPost("http://httpbin.org/", "name=value");
         */
        public void HttpPostAsync(String url_, String data_, String referer_ = "", String proxy_ = "", WebHeaderCollection headers_ = null)
        {
            BackgroundWorker bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.DoWork += new DoWorkEventHandler(
            delegate(object o, DoWorkEventArgs args)
            {
                HttpWebRequest http = (HttpWebRequest)WebRequest.Create(url_);
                if (referer_ != "" && referer_ != null) { http.Referer = referer_; }
                if (proxy_ != "" && proxy_ != null) { http = HttpProxy(http, proxy_); }
                if (headers_ != null)
                {
                    foreach (String headerStr in headers_)
                    {
                        http.Headers.Add(headerStr);
                    }
                }
                HttpPostMain(http, data_);
            });
            bw.RunWorkerAsync();
        }

        /* HttpGetMain(http)
         * - Perform an http GET request using the request object
         * - Referer, proxy, and headers parameters are optional
         * - HttpGetMain(http);
         */
        public void HttpGetMain(HttpWebRequest http)
        {
            http.Method = "GET";
            http.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            http.Headers.Add("Accept-Language", "en-US,en;q=0.5");
            http.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            if (useragent != "") { http.UserAgent = useragent; }
            else { http.UserAgent = "Mozilla/5.0 (Windows NT 6.0; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0"; }
            http.KeepAlive = true;
            
            http.CookieContainer = cookies;

            HttpWebResponse response = (HttpWebResponse)http.GetResponse();
            
            cookies = http.CookieContainer;
            
            WebHeaderCollection resheaders = response.Headers;
            int rescode = (int)response.StatusCode;
            String rescodedesc = response.StatusDescription.ToString();
            resheaders.Add("Response: " + rescode + " " + rescodedesc);
            headers = resheaders;

            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            html = reader.ReadToEnd();

            reader.Close();
            stream.Close();
            response.Close();
        }

        /* HttpPostMain(http)
         * - Perform an http POST request using the request object
         * - Referer, proxy, and headers parameters are optional
         * - HttpGetMain(http);
         */
         public void HttpPostMain(HttpWebRequest http, String data_)
        {
            http.Method = "POST";
            http.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            http.Headers.Add("Accept-Language", "en-US,en;q=0.5");
            http.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            if (useragent != "") { http.UserAgent = useragent; }
            else { http.UserAgent = "Mozilla/5.0 (Windows NT 6.0; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0"; }
            http.KeepAlive = true;
			//http.ServicePoint.Expect100Continue = false;
			http.CookieContainer = cookies;
            http = HttpData(http, data_);
            
            HttpWebResponse response = (HttpWebResponse)http.GetResponse();

            cookies = http.CookieContainer;
 
            WebHeaderCollection resheaders = response.Headers;
            int rescode = (int)response.StatusCode;
            String rescodedesc = response.StatusDescription.ToString();
            resheaders.Add("Response: " + rescode + " " + rescodedesc);
            headers = resheaders;

            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            html = reader.ReadToEnd();

            reader.Close();
            stream.Close();
            response.Close();
        }

        /* HttpData(http, data_, [contentType_])
         * - Set the POST request data headers
         * - HttpData(http, data_);
         */
        public HttpWebRequest HttpData(HttpWebRequest http, String data_, String contentType_ = "application/x-www-form-urlencoded")
        {
            http.ContentType = contentType_;
            byte[] byteArr = Encoding.UTF8.GetBytes(data_);
            http.ContentLength = byteArr.Length;
            Stream stream = http.GetRequestStream();
            stream.Write(byteArr, 0, byteArr.Length);
            stream.Close();
			return http;
        }

        /* HttpProxy(http, data_, proxy_)
         * - Set the request proxy headers
         * - HttpData(http, "125.73.2.10:8080");
         */
        public HttpWebRequest HttpProxy(HttpWebRequest http, String proxy_)
        {
            if (proxy_ != "" && proxy_ != null)
            {
                String[] proxyArr = proxy_.Split(':');
                http.Proxy = new WebProxy(proxyArr[0].ToString(), int.Parse(proxyArr[1]));
            }
			return http;
        }

        /* HttpCookiesAdd(name_, value_, domain_, [expire_])
         * - Add a cookie to the request cookie container
         * - HttpCookiesAdd("name", "value", "domain.com");
         */
        public void HttpCookiesAdd(HttpWebRequest http, String name_, String value_, String domain_, String expire_ = "")
        {
            Cookie c = new Cookie();
            c.Domain = domain_;
            c.Name = name_;
            c.Value = value_;
            c.Expires = System.DateTime.Parse(expire_);
            cookies.Add(c);
        }
    }
}
