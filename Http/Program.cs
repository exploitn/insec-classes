﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            // HttpGetAsync example 1 https://www.google.com
            Console.WriteLine("HttpGetAsync(\"https://www.google.com\")\r\nEnter to execute");
            Console.ReadLine();
            Http http = new Http();
            http.Html = "";
            http.HttpGetAsync("https://www.google.com");

            // Wait for the request to finish
            while (http.Html == "")
            {
                Thread.Sleep(50);
                Console.Write(".");
            }

            Console.WriteLine("\r\n\r\nHtml:\r\n" + http.Html.Substring(0, 500) + "\r\n");
            Console.Write("Headers:\r\n" + http.ResHeaders.ToString());
            Console.WriteLine("Cookies Count: " + http.Cookies.Count + "\r\n\r\n");

            // HttpPostAsync example 1 http://requestb.in/unx0cvun/ name=value
            Console.WriteLine("HttpPostAsync(\"http://requestb.in/unx0cvun/\", \"name=value\")\r\nHttpGetAsync(\"http://requestb.in/unx0cvun?inspect\")\r\nEnter to execute");
            Console.ReadLine();

            http.Html = "";
            http.HttpPost("http://requestb.in/unx0cvun", "name=value");

            // Wait for the http request to finish
            while (http.Html == "")
            {
                Thread.Sleep(50);
                Console.Write(".");
            }

            // HttpGetAsync request example 2
            http.Html = "";
            http.HttpGetAsync("http://requestb.in/unx0cvun?inspect");

            while (http.Html == "")
            {
                Thread.Sleep(50);
                Console.Write(".");
            }

            Console.WriteLine("\r\n\r\nHtml:\r\n" + http.Html.Substring(0, 500) + "\r\n");
            Console.Write("Headers:\r\n" + http.ResHeaders.ToString());
            Console.WriteLine("Cookies Count: " + http.Cookies.Count + "\r\n");
            Console.ReadLine();

            /* Original GET and POST method with thread pool
             *    
            Http http = new Http();
            http.Html = "";
            ThreadPool.QueueUserWorkItem(http.HttpThread, http);
            String html_ = "";
            while (html_ == "")
            {
                html_ = http.Html;
                Thread.Sleep(100);
                Console.Write(".");
            }

            Console.WriteLine("Html:\r\n" + http.Html.Substring(0, 100) + "\r\n");
            Console.Write("Headers:\r\n" + http.ResHeaders.ToString());
            Console.WriteLine("Cookies Count: " + http.Cookies.Count + "\r\n");
            Console.ReadLine();
             */

            /* Original GET and POST method examples
             *       
            Console.ReadLine();
            Http http = new Http();
            http.HttpGet("https://www.google.com");
            Console.WriteLine("Html:\r\n" + http.Html.Substring(0, 100) + "\r\n");
            Console.Write("Headers:\r\n" + http.ResHeaders.ToString());
            Console.WriteLine("Cookies Count: " + http.Cookies.Count + "\r\n");
            Console.ReadLine();
             */

            /* ExWindow class examples
             *             
            ExWindow w = new ExWindow("AIM (Precise Witem)");
            Console.WriteLine("Title: " + w.Title + "\r\nClassName: " + w.ClassName + "\r\nHandle: " + w.Handle.ToString() + "\r\nPosition: (" + w.Position.Left.ToString() + "x" + w.Position.Top.ToString() + "), (" + w.Position.Right.ToString() + "x" + w.Position.Bottom.ToString() + ")" + "\r\nClientArea: (" + w.ClientArea.Left.ToString() + "x" + w.ClientArea.Top.ToString() + "), (" + w.ClientArea.Right.ToString() + "x" + w.ClientArea.Bottom.ToString() + ")\r\n");
            for (int i = 0; i < w.Children.Count; i++)
            {
                ExWindow c = new ExWindow();
                c.GetClassName(w.Children[i]);
                c.GetName(w.Children[i]);
                c.GetPosition(w.Children[i]);
                c.Handle = w.Children[i];
                Console.WriteLine("\tTitle: " + c.Title + "\r\n\tClassName: " + c.ClassName + "\r\n\tHandle: " + c.Handle + "\r\n\tPosition: (" + c.Position.Left.ToString() + "x" + c.Position.Top.ToString() + "), (" + c.Position.Right.ToString() + "x" + c.Position.Bottom.ToString() + ")" + "\r\n\tClientArea: (" + c.ClientArea.Left.ToString() + "x" + c.ClientArea.Top.ToString() + "), (" + c.ClientArea.Right.ToString() + "x" + c.ClientArea.Bottom.ToString() + ")\r\n");
            }
            Console.WriteLine();
            Console.ReadLine();
            w = new ExWindow("Spotify");
            Console.WriteLine("Title: " + w.Title + "\r\nClassName: " + w.ClassName + "\r\nHandle: " + w.Handle.ToString() + "\r\nPosition: (" + w.Position.Left.ToString() + "x" + w.Position.Top.ToString() + "), (" + w.Position.Right.ToString() + "x" + w.Position.Bottom.ToString() + ")" + "\r\nClientArea: (" + w.ClientArea.Left.ToString() + "x" + w.ClientArea.Top.ToString() + "), (" + w.ClientArea.Right.ToString() + "x" + w.ClientArea.Bottom.ToString() + ")\r\n");
            for (int i = 0; i < w.Children.Count; i++)
            {
                ExWindow c = new ExWindow();
                c.GetClassName(w.Children[i]);
                c.GetName(w.Children[i]);
                c.GetPosition(w.Children[i]);
                c.Handle = w.Children[i];
                Console.WriteLine("\tTitle: " + c.Title + "\r\n\tClassName: " + c.ClassName + "\r\n\tHandle: " + c.Handle + "\r\n\tPosition: (" + c.Position.Left.ToString() + "x" + c.Position.Top.ToString() + "), (" + c.Position.Right.ToString() + "x" + c.Position.Bottom.ToString() + ")" + "\r\n\tClientArea: (" + c.ClientArea.Left.ToString() + "x" + c.ClientArea.Top.ToString() + "), (" + c.ClientArea.Right.ToString() + "x" + c.ClientArea.Bottom.ToString() + ")\r\n");
            }
            Console.ReadLine();
             */
        }
    }
}
