﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenPop.Mime;

namespace OpenPop_Example
{
    class Program
    {
        static void Main(string[] args)
        {
            // Set the email and pass
            // The server, port, and ssl values are determined automatically
            OpenPopClass openpopclass = new OpenPopClass("", "");

            // Get all messages and put them in a list
            List<Message> messages = new List<Message>(); 
            messages = openpopclass.MessageList(openpopclass);
            List<Mail> emails = openpopclass.Emails;

            Console.WriteLine("Emails: " + emails.Count.ToString() + "\r\n");
            Console.WriteLine("First Email Subject: " + emails[0].Subject);
            Console.ReadLine();
        }
    }
}
