﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;

using OpenPop;
using OpenPop.Common;
using OpenPop.Mime;
using OpenPop.Pop3;
using OpenPop.Pop3.Exceptions;

namespace OpenPop_Class
{
    class OpenPopClass
    {
        private String email, pass, server, port;
        private bool ssl;
        private List<Message> messages;
        private List<Mail> emails;
        private List<String> messageids;
        private List<String> seenids;
        private int messcount;

        public String Email { get { return email; } set { email = value; } }
        public String Pass { get { return pass; } set { pass = value; } }
        public List<Message> Messages { get { return messages; } set { messages = value; } }
        public List<Mail> Emails { get { return emails; } set { emails = value; } } 

        public OpenPopClss(String email_, String pass_)
        {
            if (email_.Length == 0 || pass_.Length == 0) { return; }
            if (email_.IndexOf('@') == -1 || email_.IndexOf('.') == -1) { return; }
            if (!ServerSettings(email_)) { return; }
            
            email = email_;
            pass = pass_;

            messages = new List<Message>();
            emails = new List<Mail>();
            messageids = new List<String>();
            seenids = new List<String>();
            messcount = 0;
        }

        public List<Message> MessageList(OpenPopClass openpop_)
        {
            bool newmess = true;
            seenids = new List<String>();
            List<Message> messages = new List<Message>();
            int lastcount = 0;
            int j = 0;

            String email_ = openpop_.Email;
            String pass_ = openpop_.Pass;
            if (email_.Length == 0 || pass_.Length == 0) { return messages; }
            if (email_.IndexOf('@') == -1 || email.IndexOf('.') == -1) { return messages; }
            if (!ServerSettings(email_)) { return messages; }

            // You have to reconnect several times to get all of the messages from Gmail, only 250 or so are found at once
            // Thunderbird has the same issues, have to keep clicking download all new messages, finds 250 or so at once
            int k = 0;
            while (newmess)
            {
                try
                {
                    using (Pop3Client client = new Pop3Client())
                    {
                        k++;
                        AmazonForm.statsform.SetStats(email_, messages.Count.ToString(), k.ToString());
                        List<String> messageids = new List<string>();

                        try
                        {
                            int portint = 0;

                            if (!int.TryParse(port, out portint)) { newmess = false; }

                            client.Connect(server, portint, ssl);
                            client.Authenticate(email_, pass_);
                            messageids = client.GetMessageUids();
                        }
                        catch (PopServerException ex)
                        {
                            if (ex.InnerException.Message.ToString().IndexOf("Exceeded the login limit") != -1 || ex.Message.ToString().IndexOf("ERR Exceeded the login limit") != -1)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine(ex.InnerException.Message.ToString() + "\r\n\r\n" + ex.Message.ToString() + "\r\n\r\n Exceeded the login limit. Wait 15 minutes and try again...");
                                Console.ReadLine();
                                Environment.Exit(9001);
                                //return messages;
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine(ex.InnerException.Message.ToString() + "\r\n\r\n" + ex.Message.ToString() + "\r\n\r\n Unknown OpenPop error...");
                                Console.ReadLine();
                                Environment.Exit(9001);
                                // return messages;
                            }
                        }

                        if (messageids.Count == 0) { newmess = false; }

                        Console.WriteLine("Message Ids: " + messageids.Count.ToString());

                        for (int i = 0; i < Convert.ToInt32(messageids.Count); i++)
                        {
                            string id = messageids[i];
                            if (!seenids.Contains(id))
                            {
                                // Need to thread this
                                Message message = new Message(new byte[0]);
                                try
                                {
                                    message = client.GetMessage(i + 1);
                                }
                                catch (OpenPop.Pop3.Exceptions.PopClientException pex)
                                {
                                    Console.WriteLine(" Message {" + id + "} is invalid...");
                                    break;
                                }
                                catch (System.IO.IOException iex)
                                {
                                    Console.WriteLine(" Message {" + id + "} is invalid...");
                                    break;
                                }
                                Mail mail = SetMail(message);
                                messages.Add(message);
                                emails.Add(mail);
                                seenids.Add(id);
                            }
                        }

                        Console.WriteLine(" New Messages: " + messages.Count.ToString() + "\r\n");
                        if (messages.Count > 10000) { newmess = false; }
                        if (messages.Count == lastcount)
                        {
                            j++;
                            if (j > 1) { newmess = false; }
                        }
                        else { j = 0; }

                        lastcount = messages.Count;
                    }
                    // Gmail and Hotmail temp ban POP access when you connect several times too quickly
                    // Gmail requires you to enter a captcha to ulock POP temp ban
                    // Thread.Sleep(10000);
                }
                catch (OpenPop.Pop3.Exceptions.PopClientException pex)
                {
                    Console.WriteLine(" Message is invalid...");
                }
                catch (System.IO.IOException iex)
                {
                    Console.WriteLine(" Message is invalid...");
                }
            }
            return messages;
        }

        Mail SetMail(Message message)
        {
            Mail mail;

            String body = "";
            // Load the textversion of the email if it exists
            if (message.FindFirstPlainTextVersion() != null)
            {
                body = "Text Version:\r\n\r\n" + message.FindFirstPlainTextVersion().GetBodyAsText();
            }
            // Load the html version of the email if it exists and combine it with the text version
            if (message.FindFirstHtmlVersion() != null) { body += "\r\n\r\nHtml Version:\r\n\r\n" + message.FindFirstHtmlVersion().GetBodyAsText(); }

            mail = new Mail(message.Headers.From.ToString(), message.Headers.To.ToString(), message.Headers.Subject.ToString(), body, message.Headers.DateSent);

            return mail;
        }

        // Determines and sets the POP settings based on the email domain
        bool ServerSettings(String email_)
        {
            server = "";
            port = "";
            ssl = true;

            String domain = email_.Split('@')[1];
            switch (domain)
            {
                case "outlook.com":
                {
                    HotmailSettings();
                    break;
                }
                case "hotmail.com":
                {
                    HotmailSettings();
                    break;
                }
                case "live.com":
                {
                    HotmailSettings();
                    break;
                }
                case "gmail.com":
                {
                    GmailSettings();
                    break;
                }
            }

            if (server == "" || port == "") { return false; }

            return true;
        }

        // Homtail's POP settings
        void HotmailSettings()
        {
            server = "pop3.live.com";
            port = "995";
            ssl = true;
        }

        // Gmail's POP settings
        void GmailSettings()
        {
            server = "pop.gmail.com";
            port = "995";
            ssl = true;
        }
		
		// Remove the html formatting that breaks regex from an html string
        String HtmlSafe(String html)
        {
            html = html.Replace("\r", "").Replace("\n", "").Replace("\t", "").Replace("\a", "");
            html = html.Replace("&nbsp;", " ").Replace("&nbsp", " ");
            bool blankspace = true;
            while (blankspace)
            {
                html = html.Replace("  ", " ");
                if (html.Indexof("  ") == - 1) { blankspace = false; }
            }
            return html;
        }
    }
}
