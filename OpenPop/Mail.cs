﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenPop_Example
{
    class Mail
    {
        private String from, to, subject, body;
        private DateTime sent;

        public String From { get { return from; } set { from = value; } }
        public String To { get { return to; } set { to = value; } }
        public String Subject { get { return subject; } set { subject = value; } }
        public String Body { get { return body; } set { body = value; } }
        public DateTime Sent { get { return sent; } set { sent = value; } }

        public Mail()
        {
            from = "";
            to = "";
            subject = "";
            body = "";
            sent = new DateTime();
        }

        public Mail(String from_, String to_, String subject_, String body_, DateTime sent_)
        {
            from = from_;
            to = to_;
            subject = subject_;
            body = body_;
            sent = sent_;
        }
    }
}
