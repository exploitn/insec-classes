﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Skybound.Gecko;

namespace GekoFX_Hotmail_Creator
{
    public partial class MozForm : Form
    {
        //public static GeckoWebBrowser myBrowser;

        public MozForm()
        {
            InitializeComponent();
            XulRunner();
            Browser();
        }

        public void XulRunner()
        {
            String xulPath = "xulrunner\\";
            Skybound.Gecko.Xpcom.Initialize(xulPath);
            //Skybound.Gecko.Xpcom.ProfileDirectory = "profile1\\";
        }

        public void Browser()
        {
            //myBrowser = new GeckoWebBrowser();
            //myBrowser.Show();
            //myBrowser.Parent = this;
            //myBrowser.Dock = DockStyle.Fill;
            //myBrowser.Update();
        }

        private void MozForm_Shown(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(2000);
            myBrowser.Navigate("http://google.com");
        }
    }
}
