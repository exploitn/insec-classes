﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GekoFX_Hotmail_Creator
{
    public partial class ProxyForm : Form
    {
        public static MozForm mozform;

        public ProxyForm()
        {
            InitializeComponent();
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {

        }

        // Save
        private void button1_Click(object sender, EventArgs e)
        {
            mozform = new MozForm();
            DialogResult dr = mozform.ShowDialog();
            MessageBox.Show(dr.ToString());
            this.Close();
        }
    }
}
